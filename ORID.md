# Day05-ORID

## O

a. Code Review，and learned good development habits from other members of the group.
b. A lot of TDD exercises have been carried out through Parking Lot Practice.
c. The division of team work is discussed.

## R

I was fruitful.

## I

Why focus on TDD? A good unit test can effectively reduce our development cost in the development process and reduce code bugs.

## D

Although there are many problems in today's practice, they have been successfully solved with my own thinking and the help of teachers and team members. After class, we still need to strengthen practice.

Pay attention to the naming of test functions (My own naming is not intuitive enough).







