package com.parkinglot;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class SmartParkingBoy {
    private final List<ParkingLot> parkingLotList = new ArrayList<>();
    public void addParkingLot(ParkingLot parkingLot) {
        parkingLotList.add(parkingLot);
    }
    public Ticket park(Car car) {
        return parkingLotList.stream()
                .filter(ParkingLot::isHasAvailablePosition)
                .max(Comparator.comparingInt(ParkingLot::getAvailablePositions))
                .map(parkingLot -> parkingLot.park(car))
                .orElseThrow(NoAvailablePositionException::new);
    }

    public Car fetch(Ticket ticket) {
        return parkingLotList.stream()
                .filter(parkingLot -> parkingLot.ticketIsInParkingLot(ticket))
                .map(parkingLot -> parkingLot.fetch(ticket))
                .findFirst()
                .orElseThrow(UnrecognizedTicketException::new);
    }
}
