package com.parkinglot;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SuperSmartParkingBoy {
    private List<ParkingLot> parkingLotList = new ArrayList<>();
    public void addParkingLot(ParkingLot parkingLot) {
        parkingLotList.add(parkingLot);
    }
    public Ticket park(ParkingLot parkingLot, Car car) {
        return parkingLot.park(car);
    }
    public Ticket park(Car car) {
        return parkingLotList.stream()
                .filter(parkingLot -> parkingLot.isHasAvailablePosition())
                .max(Comparator.comparingDouble(ParkingLot::getAvailablePositionsRate))
                .map(parkingLot -> parkingLot.park(car))
                .orElseThrow(NoAvailablePositionException::new);
    }

    public Car fetch(Ticket ticket) {
        return parkingLotList.stream()
                .filter(parkingLot -> parkingLot.ticketIsInParkingLot(ticket))
                .findFirst()
                .map(parkingLot -> parkingLot.fetch(ticket))
                .orElseThrow(UnrecognizedTicketException::new);
    }
}
