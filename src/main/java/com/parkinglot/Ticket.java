package com.parkinglot;

public class Ticket {
    private ParkingLot parkingLot;
    public ParkingLot getParkingLot() {
        return parkingLot;
    }

    public void setParkingLot(ParkingLot parkingLot) {
        this.parkingLot = parkingLot;
    }
}
