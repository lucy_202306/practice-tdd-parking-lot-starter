package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

public class StandardParkingBoy {
    private List<ParkingLot> parkingLotList = new ArrayList<>();

    public void addParkingLot(ParkingLot parkingLot) {
        this.parkingLotList.add(parkingLot);
    }

    public Ticket park(Car car) {
        for (ParkingLot parkingLot : parkingLotList) {
            if (parkingLot.isHasAvailablePosition()) {
                return parkingLot.park(car);
            }
        }
        throw new NoAvailablePositionException();
    }

    public Car fetch(Ticket ticket) {
        for (ParkingLot parkingLot : parkingLotList) {
            if (parkingLot.ticketIsInParkingLot(ticket)) {
                Car car = parkingLot.fetch(ticket);
                return car;
            }
        }
        throw new UnrecognizedTicketException();
    }
}
