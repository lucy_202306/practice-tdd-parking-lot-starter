package com.parkinglot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ParkingLot {
    private Map<Ticket, Car> parkingMap = new HashMap<>();
    private int maxPosition;

    public ParkingLot(int maxPosition) {
        this.maxPosition = maxPosition;
    }

    public Ticket park(Car car) {
        if (isHasAvailablePosition()) {
            Ticket ticket = new Ticket();
            parkingMap.put(ticket, car);
            ticket.setParkingLot(this);
            return ticket;
        }
        throw new NoAvailablePositionException();
    }

    public Car fetch(Ticket ticket) {
        Car car = parkingMap.get(ticket);
        if (car != null) {
            parkingMap.remove(ticket);
            return car;
        }
        throw new UnrecognizedTicketException();
    }

    public boolean ticketIsInParkingLot(Ticket ticket) {
        return parkingMap.get(ticket) != null;
    }

    public boolean isHasAvailablePosition() {
        return parkingMap.size() < maxPosition;
    }

    public int getAvailablePositions() {
        return maxPosition - parkingMap.size();
    }

    public double getAvailablePositionsRate() {
        return (maxPosition- parkingMap.size()) / maxPosition;
    }
}
