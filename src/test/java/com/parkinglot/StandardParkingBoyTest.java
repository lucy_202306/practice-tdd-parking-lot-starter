package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StandardParkingBoyTest {
    @Test
    void should_return_a_parking_ticket_when_park_the_car_given_a_parking_boy_manage_two_parking_lots_and_a_car() {
        // given
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        StandardParkingBoy parkingBoy = new StandardParkingBoy();
        parkingBoy.addParkingLot(parkingLot1);
        parkingBoy.addParkingLot(parkingLot2);

        //when
        Ticket ticket = parkingBoy.park(car);

        //then
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_park_car_to_second_parking_lot_when_fetch_the_car_given_a_parking_boy_manage_two_parking_lots_and_second_parking_lot_with_available_position_and_a_car() {
        // given
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot(5);
        ParkingLot parkingLot2 = new ParkingLot(10);
        StandardParkingBoy parkingBoy = new StandardParkingBoy();
        parkingBoy.addParkingLot(parkingLot1);
        parkingBoy.addParkingLot(parkingLot2);
        parkingBoy.park(new Car());
        parkingBoy.park(new Car());
        parkingBoy.park(new Car());
        parkingBoy.park(new Car());
        parkingBoy.park(new Car());
        parkingBoy.park(new Car());
        parkingBoy.park(new Car());
        parkingBoy.park(new Car());
        parkingBoy.park(new Car());
        parkingBoy.park(new Car());
        parkingBoy.park(new Car());
        parkingBoy.park(new Car());

        //when
        Ticket ticket = parkingBoy.park(car);

        //then
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_right_car_with_each_ticket_when_fetch_the_car_twice_given_a_parking_boy_manage_two_parking_lots_both_with_a_parked_cars_and_two_parking_tickets() {
        // given
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(10);
        StandardParkingBoy parkingBoy = new StandardParkingBoy();
        parkingBoy.addParkingLot(parkingLot1);
        parkingBoy.addParkingLot(parkingLot2);

        //when
        Ticket ticket1 = parkingBoy.park(car1);
        Ticket ticket2 = parkingBoy.park(car2);
        Car fetchCar1 = parkingBoy.fetch(ticket1);
        Car fetchCar2 = parkingBoy.fetch(ticket2);

        //then
        Assertions.assertEquals(car1, fetchCar1);
        Assertions.assertEquals(car2, fetchCar2);
    }

    @Test
    void should_return_nothing_with_error_message_when_fetch_the_car_given_a_parking_boy_manage_two_parking_lots_and_an_wrong_ticket() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        StandardParkingBoy parkingBoy = new StandardParkingBoy();
        parkingBoy.addParkingLot(parkingLot1);
        parkingBoy.addParkingLot(parkingLot2);

        //when
        UnrecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnrecognizedTicketException.class, () -> parkingBoy.fetch(new Ticket()));

        //then
        Assertions.assertEquals("Unrecognized parking ticket", unrecognizedTicketException.getMessage());
    }

    @Test
    void should_return_nothing_with_error_message_when_fetch_the_car_given_a_parking_boy_manage_two_parking_lots_and_an_used_ticket() {
        // given
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot(5);
        ParkingLot parkingLot2 = new ParkingLot(5);
        StandardParkingBoy parkingBoy = new StandardParkingBoy();
        parkingBoy.addParkingLot(parkingLot1);
        parkingBoy.addParkingLot(parkingLot2);
        Ticket ticket = parkingBoy.park(car);
        parkingBoy.fetch(ticket);

        //when
        UnrecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnrecognizedTicketException.class, () -> parkingBoy.fetch(ticket));

        //then
        Assertions.assertEquals("Unrecognized parking ticket", unrecognizedTicketException.getMessage());
    }

    @Test
    void should_return_nothing_with_error_message_when_fetch_the_car_given_a_parking_boy_manage_two_parking_lots_both_without_any_position() {
        // given
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        StandardParkingBoy parkingBoy = new StandardParkingBoy();
        parkingBoy.addParkingLot(parkingLot1);
        parkingBoy.addParkingLot(parkingLot2);
        parkingBoy.park(new Car());
        parkingBoy.park(new Car());

        //when
        NoAvailablePositionException noAvailablePositionException = Assertions.assertThrows(NoAvailablePositionException.class, () -> parkingBoy.park(car));

        //then
        Assertions.assertEquals("No Available Position", noAvailablePositionException.getMessage());
    }

}
