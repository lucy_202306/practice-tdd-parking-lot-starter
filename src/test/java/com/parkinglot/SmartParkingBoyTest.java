package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SmartParkingBoyTest {
    @Test
    void should_return_a_parking_ticket_when_park_the_car_given_a_smart_parking_boy_and_multiple_parking_lots_and_a_car() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(5);
        ParkingLot parkingLot2 = new ParkingLot(10);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();

        smartParkingBoy.addParkingLot(parkingLot1);
        smartParkingBoy.addParkingLot(parkingLot2);

        // when
        Ticket ticket = smartParkingBoy.park(new Car());

        // then
        Assertions.assertNotNull(ticket);
    }
    @Test
    void should_return_the_parked_car_when_park_the_car_given_a_smart_parking_boy_and_multiple_parking_lots_and_a_parking_ticket() {
        // given
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot(5);
        ParkingLot parkingLot2 = new ParkingLot(10);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();

        smartParkingBoy.addParkingLot(parkingLot1);
        smartParkingBoy.addParkingLot(parkingLot2);

        // when
        Ticket ticket = smartParkingBoy.park(car);
        Car fetchedCar = smartParkingBoy.fetch(ticket);

        // then
        Assertions.assertEquals(car, fetchedCar);
    }
    @Test
    void should_return_the_right_car_with_each_ticket_when_fetch_the_car_given_a_smart_parking_boy_and_multiple_parking_lots_both_with_a_parked_car_and_multiple_parking_tickets() {
        // given
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(5);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();

        smartParkingBoy.addParkingLot(parkingLot1);
        smartParkingBoy.addParkingLot(parkingLot2);
        smartParkingBoy.park(new Car());
        Ticket ticket1 = smartParkingBoy.park(car1);
        Ticket ticket2 = smartParkingBoy.park(car2);

        // when
        Car fetchedCar1 = smartParkingBoy.fetch(ticket1);
        Car fetchedCar2 = smartParkingBoy.fetch(ticket2);

        // then
        Assertions.assertEquals(parkingLot2, ticket2.getParkingLot());
        Assertions.assertNotEquals(parkingLot1, ticket2.getParkingLot());
        Assertions.assertEquals(car1, fetchedCar1);
        Assertions.assertEquals(car2, fetchedCar2);
    }
    @Test
    void should_return_nothing_with_error_message_when_fetch_the_car_given_a_smart_parking_boy_and_multiple_parking_lots_and_a_wrong_parking_tickets() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(5);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();

        smartParkingBoy.addParkingLot(parkingLot1);
        smartParkingBoy.addParkingLot(parkingLot2);

        // when
        UnrecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnrecognizedTicketException.class, ()->smartParkingBoy.fetch(new Ticket()));

        // then
        Assertions.assertEquals("Unrecognized parking ticket", unrecognizedTicketException.getMessage());
    }
    @Test
    void should_return_nothing_with_error_message_when_fetch_the_car_given_a_smart_parking_boy_and_multiple_parking_lots_and_a_used_parking_tickets() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(5);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();

        smartParkingBoy.addParkingLot(parkingLot1);
        smartParkingBoy.addParkingLot(parkingLot2);

        // when
        Ticket ticket = smartParkingBoy.park(new Car());
        smartParkingBoy.fetch(ticket);
        UnrecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnrecognizedTicketException.class, ()->smartParkingBoy.fetch(ticket));

        // then
        Assertions.assertEquals("Unrecognized parking ticket", unrecognizedTicketException.getMessage());
    }
    @Test
    void should_return_nothing_with_error_message_when_park_the_car_given_a_smart_parking_boy_and_multiple_parking_lots_both_without_position_and_a_car() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy();

        smartParkingBoy.addParkingLot(parkingLot1);
        smartParkingBoy.addParkingLot(parkingLot2);

        // when
        smartParkingBoy.park(new Car());
        smartParkingBoy.park(new Car());
        NoAvailablePositionException noAvailablePositionException = Assertions.assertThrows(NoAvailablePositionException.class, ()->smartParkingBoy.park(new Car()));

        // then
        Assertions.assertEquals("No Available Position", noAvailablePositionException.getMessage());
    }
}
