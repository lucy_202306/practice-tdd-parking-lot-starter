package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParkingLotTest {
    @Test
    void should_return_a_parking_ticket_when_park_a_car_given_a_car_and_a_parking_lot() {
        // given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(10);

        //when
        Ticket ticket = parkingLot.park(car);

        //then
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_the_parking_ticket_when_fetch_the_car_given_a_parking_ticket_and_a_parking_lot_with_a_parked_car() {
        // given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        //when
        Ticket ticket = parkingLot.park(car);
        Car fetchCar = parkingLot.fetch(ticket);

        //then
        Assertions.assertEquals(car, fetchCar);
    }

    @Test
    void should_return_right_car_with_each_ticket_when_fetch_the_car_twice_given_two_parking_tickets_and_a_parking_lot_with_two_parked_cars() {
        // given
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot = new ParkingLot(10);

        //when
        Ticket ticket1 = parkingLot.park(car1);
        Ticket ticket2 = parkingLot.park(car2);
        Car fetchCar1 = parkingLot.fetch(ticket1);
        Car fetchCar2 = parkingLot.fetch(ticket2);

        //then
        Assertions.assertEquals(car1, fetchCar1);
        Assertions.assertEquals(car2, fetchCar2);
    }

    @Test
    void should_return_nothing_with_error_message_when_fetch_the_car_given_a_parking_lot_and_a_wrong_ticket() {
        // given
        ParkingLot parkingLot = new ParkingLot(10);

        //when
        UnrecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnrecognizedTicketException.class, () -> parkingLot.fetch(new Ticket()));

        //then
        Assertions.assertEquals("Unrecognized parking ticket", unrecognizedTicketException.getMessage());
    }

    @Test
    void should_return_nothing_with_error_message_when_fetch_the_car_given_a_parking_lot_and_a_used_ticket() {
        // given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(10);

        //when
        Ticket ticket = parkingLot.park(car);
        parkingLot.fetch(ticket);
        UnrecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnrecognizedTicketException.class, () -> parkingLot.fetch(new Ticket()));

        //then
        Assertions.assertEquals("Unrecognized parking ticket", unrecognizedTicketException.getMessage());
    }

    @Test
    void should_return_nothing_with_error_message_when_park_the_car_given_a_parking_lot_without_any_position() {
        // given
        ParkingLot parkingLot = new ParkingLot(2);

        //when
        parkingLot.park(new Car());
        parkingLot.park(new Car());
        NoAvailablePositionException noAvailablePositionException = Assertions.assertThrows(NoAvailablePositionException.class, ()->parkingLot.park(new Car()));

        //then
        Assertions.assertEquals("No Available Position", noAvailablePositionException.getMessage());
    }
}
