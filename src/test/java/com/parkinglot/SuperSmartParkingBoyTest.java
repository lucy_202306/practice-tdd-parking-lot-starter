package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SuperSmartParkingBoyTest {
    @Test
    void should_return_a_parking_ticket_when_park_the_car_given_a_super_smart_parking_boy_and_a_car() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(5);
        ParkingLot parkingLot2 = new ParkingLot(10);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy();
        superSmartParkingBoy.addParkingLot(parkingLot1);
        superSmartParkingBoy.addParkingLot(parkingLot2);

        for (int i=0; i<2; i++) {
            superSmartParkingBoy.park(parkingLot1, new Car());
        }
        for (int i=0; i<6; i++) {
            superSmartParkingBoy.park(parkingLot2, new Car());
        }

        // when
        Ticket ticket = superSmartParkingBoy.park(new Car());

        // then
        Assertions.assertNotNull(ticket);
        Assertions.assertEquals(parkingLot1, ticket.getParkingLot());
    }
    @Test
    void should_return_the_parked_car_when_fetch_the_car_given_a_super_smart_parking_boy_and_a_parking_ticket() {
        // given
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot(5);
        ParkingLot parkingLot2 = new ParkingLot(10);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy();
        superSmartParkingBoy.addParkingLot(parkingLot1);
        superSmartParkingBoy.addParkingLot(parkingLot2);

        superSmartParkingBoy.park(parkingLot1, new Car());

        // when
        Ticket ticket = superSmartParkingBoy.park(car);
        Car fetchedCar = superSmartParkingBoy.fetch(ticket);

        // then
        Assertions.assertEquals(car, fetchedCar);
        Assertions.assertEquals(parkingLot2, ticket.getParkingLot());
    }
    @Test
    void should_return_the_right_car_with_each_ticket_when_fetch_the_car_given_a_super_smart_parking_boy_and_two_parking_tickets() {
        // given
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(5);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy();

        superSmartParkingBoy.addParkingLot(parkingLot1);
        superSmartParkingBoy.addParkingLot(parkingLot2);
        Ticket ticket1 = superSmartParkingBoy.park(car1);
        Ticket ticket2 = superSmartParkingBoy.park(car2);

        // when
        Car fetchedCar1 = superSmartParkingBoy.fetch(ticket1);
        Car fetchedCar2 = superSmartParkingBoy.fetch(ticket2);

        // then
        Assertions.assertEquals(car1, fetchedCar1);
        Assertions.assertEquals(car2, fetchedCar2);
        Assertions.assertEquals(parkingLot1, ticket1.getParkingLot());
        Assertions.assertNotEquals(parkingLot2, ticket1.getParkingLot());
        Assertions.assertEquals(parkingLot2, ticket2.getParkingLot());
        Assertions.assertNotEquals(parkingLot1, ticket2.getParkingLot());
    }
    @Test
    void should_return_nothing_with_error_message_when_fetch_the_car_given_a_smart_parking_boy_and_a_wrong_parking_tickets() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(5);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy();

        superSmartParkingBoy.addParkingLot(parkingLot1);
        superSmartParkingBoy.addParkingLot(parkingLot2);

        // when
        UnrecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnrecognizedTicketException.class, ()->superSmartParkingBoy.fetch(new Ticket()));

        // then
        Assertions.assertEquals("Unrecognized parking ticket", unrecognizedTicketException.getMessage());
    }
    @Test
    void should_return_nothing_with_error_message_when_fetch_the_car_given_a_smart_parking_boy_and_a_used_parking_tickets() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(5);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy();

        superSmartParkingBoy.addParkingLot(parkingLot1);
        superSmartParkingBoy.addParkingLot(parkingLot2);

        // when
        Ticket ticket = superSmartParkingBoy.park(new Car());
        superSmartParkingBoy.fetch(ticket);
        UnrecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnrecognizedTicketException.class, ()->superSmartParkingBoy.fetch(ticket));

        // then
        Assertions.assertEquals("Unrecognized parking ticket", unrecognizedTicketException.getMessage());
    }
    @Test
    void should_return_nothing_with_error_message_when_fetch_the_car_given_a_smart_parking_boy_without_any_position_and_a_car() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(3);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy();
        superSmartParkingBoy.addParkingLot(parkingLot1);
        superSmartParkingBoy.addParkingLot(parkingLot2);

        // when
        for (int i = 0; i < 5; i++) {
            superSmartParkingBoy.park(new Car());
        }
        NoAvailablePositionException noAvailablePositionException = Assertions.assertThrows(NoAvailablePositionException.class, ()->superSmartParkingBoy.park(new Car()));

        // then
        Assertions.assertEquals("No Available Position", noAvailablePositionException.getMessage());
    }
}
